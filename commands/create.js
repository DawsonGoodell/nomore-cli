var fs = require('fs-extra');

var chalk = require('chalk');
var child = require('child_process');
var Spinner = require('cli-spinner').Spinner;

var utils = require('./lib/utils');

module.exports = function(opts) {
  var directory = opts._args[1];
  if (opts.help) {
    console.log('Creates a new NoMoRe project in a given directory by cloning the base git repository.');
    return;
  }

  if (!directory) {
    console.log(chalk.red('You need to pass an empty directory to clone into.'));
    return;
  }


  //clone the repo here
  const gitClone = child.spawn('git', ['clone', 'git@bitbucket.org:DawsonGoodell/nomore.git', directory]);
  var spinner = new Spinner('processing %s');
  spinner.setSpinnerString('|/-\\');
  spinner.start();

  gitClone.stdout.on('data', data => {
    console.log(`stdout: ${data}`);
  });

  gitClone.stderr.on('data', data => {
    console.log(`${data}`);
  });

  gitClone.on('close', code => {
    spinner.stop();

    if (code != 0) {
      console.log(chalk.red(`child process exited with code ${code}`));
      return;
    }

    console.log(chalk.green(`\nEverything went to plan! Your project has been created in ${directory}.`));
    console.log(`\nBut we aren't done.  Time to cleanup the repo and move your console.`);

    process.chdir(directory);

    // Remove the current git folder
    child.exec(`rm -Rf .git`, (err, stdout, stderr) => {
      if (err) console.log(err);
      if (stdout) console.log(stdout);
      if (stderr) console.log(stderr);

      console.log(`\nInitializing a new git repo! (Not using git? Huh... weird.)`);
      utils.announceCommand('git init', child.execSync);
      console.log(`\nAdding an 'original' remote.`);
      utils.announceCommand('git remote add original git@bitbucket.org:DawsonGoodell/nomore.git', child.execSync);
      utils.announceCommand('git fetch original', child.execSync);

      console.log(chalk.green(`\nOK! Now we're done.  Your project is in ./${directory}.`));
    });
  });
}
