var chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;

var gServerController = require('./generate/server-controller.js');
var gServerModel = require('./generate/server-model.js');

module.exports = function(opts) {
  var type = opts._args[1];
  var name = opts._args[2];

  switch (type) {
    case 'controller':
    case 'server-controller':
      gServerController(name);

      break;
    case 'model':
    case 'server-model':
      gServerModel(name);

      break;
  }
};
