var fs = require('fs-extra');
var chalk = require('chalk');

var utils = require('../lib/utils');

const path = `${process.cwd()}/server/`;

module.exports = function(name) {
  var lowercaseName = name.toLowerCase();
  var capitalizedName = utils.capitalize(name);

  const controllerSource = `import BaseController from './base.controller';

class ${capitalizedName}Controller extends BaseController {
  get${capitalizedName}(req, res) {
    return res.json({ success: true });
  }
}

export default new ${capitalizedName}Controller();
`;

  const routesSource = `import { Router } from 'express';
import ${capitalizedName}Controller from '../controllers/${lowercaseName}.controller';

const router = new Router();

router.route('/get${capitalizedName}').get(${capitalizedName}Controller.get${capitalizedName});

export default router;
`;

  console.log('We are creating your controller.  Please wait.');
  fs.outputFile(`${path}/controllers/${lowercaseName}.controller.js`, controllerSource, (err) => {
    if (err) {
      return utils.showError(err);
    }

    console.log(chalk.green(`${capitalizedName}Controller created!`));
  });

  console.log("Adding routes");
  fs.outputFile(`${path}/routes/${lowercaseName}.routes.js`, routesSource, (err) => {
    if (err) {
      return utils.showError(err);
    }

    console.log(chalk.green(`${capitalizedName} routing file created!`));
  });

  console.log("Adding routes to master route file");
  fs.readFile(`${path}/routes/index.js`, (err, data) => {
    if (err) {
      return utils.showError(err);
    }

    var lastImport = 0;
    var lastRouterUse = 0;
    var lines = data.toString().split("\n");
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];

      if (line.startsWith('import ')) {
        lastImport = i+1;
      }

      if (line.startsWith('router.use(')) {
        lastRouterUse = i+1;
      }
    }

    lines.splice(lastImport, 0, `import ${lowercaseName} from './${lowercaseName}.routes';`);
    lines.splice(lastRouterUse, 0, `router.use('/${lowercaseName}', ${lowercaseName});`);
    fs.writeFile(`${path}/routes/index.js`, lines.join("\n"), (err) => {
      console.log(chalk.green("wOOt!"));
    });
  });
}
