var fs = require('fs-extra');
var chalk = require('chalk');

var utils = require('../lib/utils');

const path = `${process.cwd()}/server/`;

module.exports = function(name) {
  var lowercaseName = name.toLowerCase();
  var capitalizedName = utils.capitalize(name);

  const modelSource = `import mongoose from 'mongoose';

const ${lowercaseName}Schema = new mongoose.Schema({}, { timestamps: true });

const ${capitalizedName} = mongoose.model('${capitalizedName}', ${lowercaseName}Schema);

export default ${capitalizedName};
`;

  console.log('We are creating your model.  Please wait.');
  fs.outputFile(`${path}/models/${capitalizedName}.js`, modelSource, (err) => {
    if (err) {
      return utils.showError(err);
    }

    console.log(chalk.green(`${capitalizedName} model created!`));
  });
};
