var chalk = require('chalk');

module.exports = {
  capitalize: function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
  },

  showError: function(err) {
    console.log(chalk.red("An error occurred.  See below:"));
    console.log(err);
    return false;
  },

  announceCommand: function(cmd, exec) {
    console.log(chalk.blue(cmd));
    exec(cmd);
  }
};
