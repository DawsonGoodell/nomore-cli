#!/usr/bin/env node --harmony

const dashdash = require('dashdash');

const options = [
  {
    name: "help",
    type: "bool",
    description: "Displays a help file instead of running the command."
  }
];

var parser = dashdash.createParser({options: options});
try {
    var opts = parser.parse(process.argv);
} catch (e) {
    console.error('Error: %s', e.message);
    process.exit(1);
}

switch (opts._args[0]) {
  case "create":
  case "new":
    require('./commands/create.js')(opts);
    break;
  case "generate":
  case "g":
    require('./commands/generate.js')(opts);
    break;
}
